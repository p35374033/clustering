# Clustering

En este segundo TP de la materia de Programación III de la UNGS, comenzamos a trabajar con grafos. El objetivo de la aplicación desarrollada es poder identificar automáticamente grupos de personas en base a sus características, y poder obtener algunas conclusiones sobre los intereses de los grupos en sí. La interfaz desarrollada con bibliotecas de interfaz gráfica “Java Swing”, nos permite cargar personas y sus intereses, para luego poder pedir una cantidad de grupos según la necesidad, y poder visualizar algunos detalles de los mismos, como por ejemplo, estadísticas sobre promedio de intereses, interés más destacado, etc.

## Authors and acknowledgment

**Juan Maria Latasa**  <span style="color:blue">@KianLatas</span>

**Nicolas Rodriguez**  <span style="color:blue">@Nico.Rodriguez</span>.

**Juan Ignacio Irigoin**  <span style="color:blue">@jirigoinaa</span>.

