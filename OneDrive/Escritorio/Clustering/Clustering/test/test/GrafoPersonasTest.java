package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import clustering.GrafoPersonas;
import clustering.ListaDePersonas;
import clustering.Persona;

class GrafoPersonasTest {

	private GrafoPersonas grafo;

	private ListaDePersonas listaPersonas;

	@BeforeEach /*-Configurar el grafo y la lista de personas antes de cada prueba.*/
	public void setUp() {
		Persona persona1 = new Persona("Persona1", 3, 4, 5, 2);
		Persona persona2 = new Persona("Persona2", 2, 4, 5, 3);
		Persona persona3 = new Persona("Persona3", 2, 4, 5, 3);
		listaPersonas = new ListaDePersonas();
		listaPersonas.agregarPersonaALista(persona1);
		listaPersonas.agregarPersonaALista(persona2);
		listaPersonas.agregarPersonaALista(persona3);

		grafo = new GrafoPersonas(listaPersonas);
	}

	@Test
	public void testObtenerIndiceSimilaridad() {
		// Given
		Persona p0 = listaPersonas.obtenerPersona(0);

		// When
		int indiceSimilaridadEntreP0yP1 = p0.indiceSimilaridad(listaPersonas.obtenerPersona(1));

		// Then
		assertEquals(indiceSimilaridadEntreP0yP1, grafo.obtenerIndiceSimilaridad(0, 1));
	}

	@Test
	public void testEliminarAristaMayorPeso() {
		// Given
		int verticeA = 0;
		int verticeB = 1;
		int pesoAristaPreborrado = grafo.obtenerIndiceSimilaridad(verticeA, verticeB);
		int expectedResult = -1;

		// When
		grafo.eliminarRelacionConDiferenciaIndiceMasAmplio();

		// Then
		int result = grafo.obtenerIndiceSimilaridad(0, 1);
		assertNotEquals(pesoAristaPreborrado, result);
		assertEquals(expectedResult, result);
		assertFalse(grafo.existeRelacion(verticeA, verticeB));
	}

	@Test
	public void testListaConUnSoloElemento() {
		Persona persona3 = new Persona("Persona3", 2, 4, 5, 3);
		listaPersonas = new ListaDePersonas();
		listaPersonas.agregarPersonaALista(persona3);

		assertThrows(IllegalArgumentException.class, () -> {
			new GrafoPersonas(listaPersonas);
		});
	}

	@Test
	public void testObtenerIndiceConXYIguales() {
		assertThrows(IllegalArgumentException.class, () -> {
			grafo.obtenerIndiceSimilaridad(0, 0);
		});
	}

	@Test
	public void testObtenerIndiceConXYNegativos() {
		assertThrows(IllegalArgumentException.class, () -> {
			grafo.obtenerIndiceSimilaridad(-1, -1);
		});
	}

	@Test
	public void testObtenerIndiceConXYExcediendoLimites() {
		int outBound = listaPersonas.cantidadPersonasEnLista();

		assertThrows(IllegalArgumentException.class, () -> {
			grafo.obtenerIndiceSimilaridad(0, outBound);
		});
	}

	@Test
	public void testAgregarPersonaAlGrafo() {
		// Given
		String nombreNuevaPersona = "un nombre super original";
		Persona nuevaPersona = new Persona(nombreNuevaPersona, 1, 1, 1, 1);
		int tamanioListaOriginal = grafo.getListaPersonas().cantidadPersonasEnLista();

		// When
		grafo.agregarPersonaGrafo(nuevaPersona);

		// Then
		assertNotEquals(tamanioListaOriginal, grafo.getListaPersonas().cantidadPersonasEnLista());
		assertEquals(tamanioListaOriginal + 1, grafo.getListaPersonas().cantidadPersonasEnLista());
		assertTrue(grafo.getListaPersonas().existePersona(nombreNuevaPersona));
	}

	@Test
	public void separarEnDosGrupos() {
		// Given
		Persona persona1 = new Persona("Con mayor diferencia interes", 5, 5, 5, 5);
		Persona persona2 = new Persona("Intereses Similares", 1, 1, 1, 1);
		Persona persona3 = new Persona("Intereses similares con el de arriba", 2, 2, 2, 2);
		ListaDePersonas listaPersonas = new ListaDePersonas();
		listaPersonas.agregarPersonaALista(persona1);
		listaPersonas.agregarPersonaALista(persona2);
		listaPersonas.agregarPersonaALista(persona3);

		grafo = new GrafoPersonas(listaPersonas);

		ListaDePersonas grupoEsperadoUno = new ListaDePersonas();
		ListaDePersonas grupoEsperadoDos = new ListaDePersonas();
		String nombrePersonaAislada = grafo.getListaPersonas().obtenerPersona(0).getNombre();

		for (Persona persona : grafo.getListaPersonas().getPersonas()) {
			if (persona.getNombre().equals(nombrePersonaAislada)) {
				grupoEsperadoUno.agregarPersonaALista(persona);
			} else {
				grupoEsperadoDos.agregarPersonaALista(persona);
			}
		}

		// When
		Set<ListaDePersonas> grupos = grafo.separarEnGrupos(2);

		// Then
		assertEquals(2, grupos.size());
		for (ListaDePersonas personas : grupos) {
			assertFalse(personas.getPersonas().isEmpty());
		}
		for (ListaDePersonas personas : grupos) {
			if (personas.existePersona(nombrePersonaAislada)) {
				assertEquals(grupoEsperadoUno.getPersonas().size(), personas.getPersonas().size());
			} else {
				assertEquals(grupoEsperadoDos.getPersonas().size(), personas.getPersonas().size());
				assertTrue(personas.existePersona(grafo.getListaPersonas().obtenerPersona(1).getNombre()));
				assertTrue(personas.existePersona(grafo.getListaPersonas().obtenerPersona(2).getNombre()));
			}
		}
	}

	@Test
	public void separarEnMismaCantidadDeGruposQueLosVerticesDelGrafoOriginal() {
		// Given
		int cantidadDeVerticesDelGrafo = grafo.tamanio();

		// When
		Set<ListaDePersonas> grupos = grafo.separarEnGrupos(cantidadDeVerticesDelGrafo);

		// Then
		assertEquals(cantidadDeVerticesDelGrafo, grupos.size());
	}

	@Test
	public void separarEnGruposMayorACantidadDeVertices() {
		// Given
		int tamanioExcesivo = grafo.tamanio() + 1;

		// When
		Executable executable = () -> grafo.separarEnGrupos(tamanioExcesivo);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

}
