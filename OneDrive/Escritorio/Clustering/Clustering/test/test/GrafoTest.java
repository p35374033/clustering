package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import clustering.Grafo;

public class GrafoTest {

	private Grafo grafo;

	@BeforeEach /*-Configurar el grafo y la lista de personas antes de cada prueba.*/
	public void setUp() {
		grafo = new Grafo(3);

		// 0 -> 1
		grafo.agregarArista(4, 0, 1);
		// 0 ->2
		grafo.agregarArista(2, 0, 2);
		// 1-> 2
		grafo.agregarArista(5, 1, 2);
	}

	@Test
	public void testAgregarAristaOk() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeA = 0;
		int verticeB = 2;

		// When
		grafo.agregarArista(3, verticeA, verticeB);

		// Then
		assertTrue(grafo.existeArista(verticeA, verticeB));
	}

	@Test
	public void testAgregarAristaCircular() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeA = 0;
		int verticeB = 0;

		// When
		Executable executable = () -> grafo.agregarArista(3, verticeA, verticeB);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testAgregarAristaPrimerVerticeFueraDeRango() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeFueraDeRango = -1;
		int verticeB = 0;

		// When
		Executable executable = () -> grafo.agregarArista(3, verticeFueraDeRango, verticeB);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testAgregarAristaSegundoVerticeFueraDeRango() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeFueraDeRango = 3;
		int verticeA = 0;

		// When
		Executable executable = () -> grafo.agregarArista(3, verticeA, verticeFueraDeRango);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testEliminarAristaOk() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeA = 0;
		int verticeB = 2;
		grafo.agregarArista(3, verticeA, verticeB);

		// When
		grafo.eliminarArista(verticeA, verticeB);

		// Then
		assertFalse(grafo.existeArista(verticeA, verticeB));
	}

	@Test
	public void testEliminarAristaPrimerVerticeFueraDeRango() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeFueraDeRango = 3;
		int verticeB = 0;

		// When
		Executable executable = () -> grafo.eliminarArista(verticeFueraDeRango, verticeB);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testEliminarAristaSegundoVerticeFueraDeRango() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeFueraDeRango = 3;
		int verticeA = 0;

		// When
		Executable executable = () -> grafo.eliminarArista(verticeA, verticeFueraDeRango);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testExisteAristaAristaExistente() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeA = 0;
		int verticeB = 2;
		grafo.agregarArista(3, verticeA, verticeB);

		// Then
		assertTrue(grafo.existeArista(verticeA, verticeB));
	}

	@Test
	public void testExisteAristaAristaInexistente() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeA = 0;
		int verticeB = 2;

		// Then
		assertFalse(grafo.existeArista(verticeA, verticeB));
	}

	@Test
	public void testExisteAristaAristaVerticeXNegativo() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeA = -1;
		int verticeB = 2;

		// When
		Executable executable = () -> grafo.existeArista(verticeA, verticeB);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testExisteAristaAristaVerticeYNegativo() {
		// Given
		Grafo grafo = new Grafo(3);
		int verticeA = 2;
		int verticeB = -1;

		// When
		Executable executable = () -> grafo.existeArista(verticeA, verticeB);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testVecinosOK() {
		// Given
		int vertice = 0;
		int vecinosEsperados = grafo.tamanio() - 1;
		int vecinoUno = 1;
		int vecinoDos = 2;

		// When
		Set<Integer> vecinos = grafo.vecinos(vertice);

		// Then
		assertEquals(vecinosEsperados, vecinos.size());
		assertFalse(vecinos.contains(vertice));
		assertTrue(vecinos.contains(vecinoUno));
		assertTrue(vecinos.contains(vecinoDos));
	}

	@Test
	public void testVecinosVerticeInvalido() {
		// Given
		int verticeInvalido = 1000;

		// When
		Executable executable = () -> grafo.vecinos(verticeInvalido);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void existeCaminoEntreVerticesOK() {
		// Given
		grafo = new Grafo(3);
		int verticeOrigen = 0;
		int verticeIntermedio = 1;
		int verticeALlegar = 2;
		grafo.agregarArista(0, verticeOrigen, verticeIntermedio);
		grafo.agregarArista(0, verticeIntermedio, verticeALlegar);

		// When
		boolean llega = grafo.existeCaminoEntreVertices(verticeOrigen, verticeALlegar);

		// Then
		assertTrue(llega);
	}

	@Test
	public void existeCaminoEntreVerticesIgualVerticeOrigen() {
		grafo = new Grafo(3);
		int verticeOrigen = 2;
		int verticeALlegar = 2;

		// When
		boolean llega = grafo.existeCaminoEntreVertices(verticeOrigen, verticeALlegar);

		// Then
		assertTrue(llega);
	}

	@Test
	public void existeCaminoEntreVerticesVerticeAislado() {
		// Given
		grafo = new Grafo(3);
		int verticeDesconectado = 0;
		int verticeALlegar = 2;

		// When
		boolean llega = grafo.existeCaminoEntreVertices(verticeDesconectado, verticeALlegar);

		// Then
		assertFalse(llega);
	}

	@Test
	public void obtenerArbolGeneradorMinimo() {
		Grafo grafoOriginal = new Grafo(3);
		// 0 -> 1
		grafoOriginal.agregarArista(4, 0, 1);
		// 0 ->2
		grafoOriginal.agregarArista(2, 0, 2);
		// 1-> 2
		grafoOriginal.agregarArista(5, 1, 2);

		Grafo grafoEsperado = grafoOriginal;
		grafoEsperado.eliminarArista(1, 2);// La arista (1,2) no existirá en el AGM ya que es la de mayor peso y con las
		// otras dos ya puedo tener el grafo conexo.

		// When
		Grafo arbolGeneradorMinimo = grafo.obtenerArbolGeneradorMinimo();

		// Then
		assertNotNull(arbolGeneradorMinimo);
		assertEquals(grafo.tamanio(), arbolGeneradorMinimo.tamanio());
		assertEquals(grafoEsperado.vecinos(0), arbolGeneradorMinimo.vecinos(0));
		assertEquals(grafoEsperado.vecinos(1), arbolGeneradorMinimo.vecinos(1));
		assertEquals(grafoEsperado.vecinos(2), arbolGeneradorMinimo.vecinos(2));
	}

	@Test
	public void obtenerArbolGeneradorMinimoVerificarConexionEntreTodosLosVertices() {
		// When
		Grafo arbolGeneradorMinimo = grafo.obtenerArbolGeneradorMinimo();

		// Then
		for (int i = 0; i < arbolGeneradorMinimo.tamanio(); i++) {
			int verticeOrigen = i;
			for (int j = 0; j < arbolGeneradorMinimo.tamanio(); j++) {
				int verticeALlegar = j;
				if (verticeOrigen == verticeALlegar)
					continue;
				boolean[] visitados = new boolean[grafo.tamanio()];
				assertTrue(arbolGeneradorMinimo.existeCaminoEntreVertices(verticeOrigen, verticeALlegar));
			}
		}
	}
}
