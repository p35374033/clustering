package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import clustering.ListaDePersonas;
import clustering.Persona;
import clustering.Persona.Intereses;

class ListaDePersonasTest {

	private ListaDePersonas lista;

	@BeforeEach
	public void setUp() {
		lista = new ListaDePersonas();
	}

	@Test
	public void testAgregarPersonaALista() {
		Persona persona = new Persona("Juan", 2, 3, 1, 4);

		lista.agregarPersonaALista(persona);

		assertEquals(1, lista.cantidadPersonasEnLista());
	}

	@Test
	public void testCantidadPersonasEnLista() {
		assertEquals(0, lista.cantidadPersonasEnLista());
		Persona persona1 = new Persona("Pepe", 1, 3, 2, 5);
		Persona persona2 = new Persona("Claudio", 3, 2, 1, 3);
		int expected = 2;

		lista.agregarPersonaALista(persona1);
		lista.agregarPersonaALista(persona2);

		assertEquals(expected, lista.cantidadPersonasEnLista());
	}

	@Test
	public void testObtenerPersona() {
		Persona persona = new Persona("Carlos", 2, 1, 4, 4);

		lista.agregarPersonaALista(persona);

		assertEquals(persona, lista.obtenerPersona(0));
	}

	@Test
	public void testObtenerPersonaConIndiceNegativo() {
		assertThrows(IllegalArgumentException.class, () -> {
			Persona persona = new Persona("Eri", 2, 3, 4, 1);
			Persona persona2 = new Persona("Chacho", 2, 3, 4, 1);
			lista.agregarPersonaALista(persona);
			lista.agregarPersonaALista(persona2);

			lista.obtenerPersona(-2);
		});
	}

	@Test
	public void testObtenerPersonaConIndiceMayorATamanioLista() {
		assertThrows(IllegalArgumentException.class, () -> {
			Persona persona = new Persona("Eri", 2, 3, 4, 1);
			Persona persona2 = new Persona("Chacho", 2, 3, 4, 1);
			lista.agregarPersonaALista(persona);
			lista.agregarPersonaALista(persona2);

			lista.obtenerPersona(3);
		});
	}

	@Test
	public void testObtenerPersonaPorNombre() {
		Persona persona = new Persona("David Nalbandian", 2, 3, 4, 1);

		lista.agregarPersonaALista(persona);

		assertEquals(persona, lista.obtenerPersonaPorNombre("David Nalbandian"));
	}

	@Test
	public void testObtenerPersonaQueNoExistePorNombre() {
		// When
		Persona persona = lista.obtenerPersonaPorNombre("una persona que no existe");

		assertTrue(Objects.isNull(persona));
	}

	@Test
	public void testObtenerPersonaPorNombreVacio() {
		assertThrows(IllegalArgumentException.class, () -> {
			Persona persona = new Persona("", 2, 3, 4, 1);
			lista.agregarPersonaALista(persona);

			lista.obtenerPersonaPorNombre("");
		});
	}

	@Test
	public void testObtenerPersonaPorNombreEspaciosEnBlanco() {
		// When
		Executable executable = () -> lista.obtenerPersonaPorNombre("   ");

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testObtenerPersonaPorNombreNull() {
		// When
		Executable executable = () -> lista.obtenerPersonaPorNombre(null);

		// Then
		assertThrows(IllegalArgumentException.class, executable);
	}

	@Test
	public void testExistePersonaEnLista() {
		Persona persona = new Persona("Pity", 2, 1, 5, 5);

		lista.agregarPersonaALista(persona);

		assertTrue(lista.existePersona("Pity"));
	}

	@Test
	public void testNoExistePersonaEnLista() {
		Persona persona = new Persona("Paula", 1, 2, 3, 4);

		lista.agregarPersonaALista(persona);

		assertFalse(lista.existePersona("Pepe"));
	}

	@Test
	public void testDeportePromedio() {
		setUpPromedios();

		float esperado = 3;

		assertEquals(lista.deportesPromedio(), esperado, 0.001);
	}

	@Test
	public void testMusicaPromedio() {
		setUpPromedios();

		float esperado = 3;

		assertEquals(lista.musicaPromedio(), esperado, 0.001);
	}

	@Test
	public void testEspectaculosPromedio() {
		setUpPromedios();

		float esperado = 3;

		assertEquals(lista.espectaculosPromedio(), esperado, 0.001);
	}

	@Test
	public void testCienciaPromedio() {
		setUpPromedios();

		float esperado = 3;

		assertEquals(lista.cienciaPromedio(), esperado, 0.001);
	}

	@Test
	public void testInteresPrincipalDeportes() {
		Persona persona = new Persona("Pers1", 5, 2, 2, 2);
		Persona persona2 = new Persona("Pers2", 5, 2, 2, 2);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);

		Intereses esperado = Intereses.DEPORTES;

		assertEquals(lista.interesPrincipal(), esperado);
	}

	@Test
	public void testInteresPrincipalMusica() {
		Persona persona = new Persona("Pers1", 2, 5, 2, 2);
		Persona persona2 = new Persona("Pers2", 2, 5, 2, 2);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);

		Intereses esperado = Intereses.MUSICA;

		assertEquals(lista.interesPrincipal(), esperado);
	}

	@Test
	public void testInteresPrincipalEspectaculos() {
		Persona persona = new Persona("Pers1", 2, 2, 5, 2);
		Persona persona2 = new Persona("Pers2", 2, 2, 5, 2);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);

		Intereses esperado = Intereses.ESPECTACULOS;

		assertEquals(lista.interesPrincipal(), esperado);
	}

	@Test
	public void testInteresPrincipalCiencia() {
		Persona persona = new Persona("Pers1", 2, 2, 2, 5);
		Persona persona2 = new Persona("Pers2", 2, 2, 2, 5);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);

		Intereses esperado = Intereses.CIENCIAS;

		assertEquals(lista.interesPrincipal(), esperado);
	}

	@Test
	public void testPromedioInteresCiencia() {
		// Given
		int interesCienciaP1 = 5;
		int interesCienciaP2 = 3;
		float promedio = (float) (interesCienciaP1 + interesCienciaP2) / 2;
		Persona persona = new Persona("Pers1", 1, 1, 1, interesCienciaP1);
		Persona persona2 = new Persona("Pers2", 1, 1, 1, interesCienciaP2);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);

		float resultado = lista.promedioInteres(Intereses.CIENCIAS);

		assertEquals(promedio, resultado, 0.001);
	}

	@Test
	public void testPromedioInteresMusica() {
		// Given
		int interesMusicaP1 = 4;
		int interesMusicaP2 = 2;
		float promedio = (float) (interesMusicaP1 + interesMusicaP2) / 2;
		Persona persona = new Persona("Pers1", 1, interesMusicaP1, 1, 1);
		Persona persona2 = new Persona("Pers2", 1, interesMusicaP2, 1, 1);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);

		float resultado = lista.promedioInteres(Intereses.MUSICA);

		assertEquals(promedio, resultado, 0.001);
	}

	@Test
	public void testPromedioInteresEspectaculos() {
		// Given
		int interesEspectaculosP1 = 2;
		int interesEspectaculosP2 = 1;
		float promedio = (float) (interesEspectaculosP1 + interesEspectaculosP2) / 2;
		Persona persona = new Persona("Pers1", 1, 1, interesEspectaculosP1, 1);
		Persona persona2 = new Persona("Pers2", 1, 1, interesEspectaculosP2, 1);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);

		float resultado = lista.promedioInteres(Intereses.ESPECTACULOS);

		assertEquals(promedio, resultado, 0.001);
	}

	@Test
	public void testPromedioInteresDeportes() {
		// Given
		int interesDeportesP1 = 5;
		int interesDeportesP2 = 5;
		float promedio = (float) (interesDeportesP1 + interesDeportesP2) / 2;
		Persona persona = new Persona("Pers1", interesDeportesP1, 1, 1, 1);
		Persona persona2 = new Persona("Pers2", interesDeportesP2, 1, 1, 1);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);

		float resultado = lista.promedioInteres(Intereses.DEPORTES);

		assertEquals(promedio, resultado, 0.001);
	}

	private void setUpPromedios() {
		Persona persona = new Persona("Pers1", 2, 2, 2, 2);
		Persona persona2 = new Persona("Pers2", 4, 4, 4, 4);
		lista.agregarPersonaALista(persona);
		lista.agregarPersonaALista(persona2);
	}

}
