package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import clustering.Persona;

class PersonaTest {
	@Test
	public void testIndicesIgualesEntrePersonas() {
		Persona p1 = new Persona("Persona1", 3, 4, 5, 2);
		Persona p2 = new Persona("Persona2", 3, 4, 5, 2);

		assertEquals(p1.indiceSimilaridad(p2), 0);
	}

	@Test
	public void testMaximaDiferenciaDeIndicesEntrePersonas() {
		Persona p1 = new Persona("Persona1", 1, 1, 1, 1);
		Persona p2 = new Persona("Persona2", 5, 5, 5, 5);

		assertEquals(p1.indiceSimilaridad(p2), 16);
	}

	/*-verificar nombre no vacío*/
	@Test
	public void testNombreVacio() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Persona("", 3, 4, 5, 2);
		});
	}

	@Test
	public void testDeportesFueraDeRango() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Persona("Nico", 0, 4, 5, 2);
		});
	}

	@Test
	public void testMusicaFueraDeRango() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Persona("Carlos", 3, 6, 5, 2);
		});
	}

	@Test
	public void testEspectaculosFueraDeRango() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Persona("Luis", 3, 4, 6, 2);
		});
	}

	@Test
	public void testCienciasFueraDeRango() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Persona("Demichelis", 3, 4, 5, 0);
		});
	}
}
