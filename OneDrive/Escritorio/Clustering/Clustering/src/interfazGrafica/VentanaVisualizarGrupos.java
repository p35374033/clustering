package interfazGrafica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JFrame;

import clustering.GrafoPersonas;
import clustering.ListaDePersonas;
import clustering.Persona;

public class VentanaVisualizarGrupos extends JFrame {

	private Set<ListaDePersonas> grupos;

	private ArrayList<ListaDePersonas> gruposParaMostrar = new ArrayList<>();

	public VentanaVisualizarGrupos(ListaDePersonas listaPersonas, int cantidad) {
		grupos = new GrafoPersonas(listaPersonas).separarEnGrupos(cantidad);
		int anchoVentana = grupos.size() * 220;
		Common.inicializarVentana(this, anchoVentana);
		for (ListaDePersonas grupo : grupos) {
			this.gruposParaMostrar.add(grupo);

		}
		crearVentanaVisualizarGrupos();
	}

	private void crearVentanaVisualizarGrupos() {
		int corrimiento = 12;
		int y = 64;
		int ancho = 200;
		int alto = 92;
		int numeroParaGrupos = 1;

		for (ListaDePersonas grupo : gruposParaMostrar) {
			agregarLabels(corrimiento, grupo, numeroParaGrupos);
			agregarTextos(grupo, corrimiento, y, ancho, alto);
			crearBotonGrupo(grupo, corrimiento);

			corrimiento += 216;
			numeroParaGrupos++;
		}

		botonCerrar();
	}

	private void crearBotonGrupo(ListaDePersonas grupo, int x) {
		ActionListener listener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaVisualizarGruposDetallado visualizarGrupo = new VentanaVisualizarGruposDetallado(grupo);
				visualizarGrupo.setVisible(true);
			}
		};
		Common.agregarBoton(this, "Ver detalles", listener, x, 200, 150, 25);
	}

	private void agregarLabels(int pos, ListaDePersonas lista, int numeroGrupo) {
		Common.agregarLabel(this, "Grupo " + String.valueOf(numeroGrupo), pos, 12, 70, 15);
		Common.agregarLabel(this, "Integrantes:", pos, 37, 110, 15);
		Common.agregarLabel(this, "Mayor interes: ", pos, 155, 200, 20);
		Common.agregarLabel(this, lista.interesPrincipal().name(), pos, 175, 200, 20);
	}

	private void botonCerrar() {
		Common.agregarBotonCerrar(this);
	}

	private void agregarTextos(ListaDePersonas grupo, int x, int y, int width, int height) {
		Common.agregarCuadroDeTexto(this, presentarPersonas(grupo), x, y, width, height);
	}

	private String presentarPersonas(ListaDePersonas listaPersonas) {
		StringBuilder personas = new StringBuilder();
		for (Persona persona : listaPersonas.getPersonas()) {
			personas.append(persona.getNombre()).append("\n");
		}
		return personas.toString();
	}
}
