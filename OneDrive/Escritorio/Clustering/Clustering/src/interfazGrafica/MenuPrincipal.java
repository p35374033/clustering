package interfazGrafica;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.ToolTipManager;
import javax.swing.JFrame;

import clustering.ListaDePersonas;

public class MenuPrincipal {

	private JFrame menuPrincipal;

	private ListaDePersonas listaDePersonas;

	private int cantidadDeGrupos = 2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal window = new MenuPrincipal();
					window.menuPrincipal.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MenuPrincipal() {
		listaDePersonas = new ListaDePersonas();
		initialize();
	}

	private void initialize() {
		menuPrincipal = new JFrame();
		Common.inicializarVentana(menuPrincipal);
		inicializarMenuCantidadDeGrupos();
		crearBotones();
		crearLabels();
	}

	private void crearBotones() {
		botonCargarDatos();
		botonVisualizarDatos();
		botonCrearGrupos();
	}

	private void botonCargarDatos() {
		ActionListener listener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaCargarDatos ventanaCargarDatos = new VentanaCargarDatos(listaDePersonas);
				ventanaCargarDatos.setVisible(true);
			}
		};
		Common.agregarBoton(menuPrincipal, "Cargar datos", listener, 98, 112, 224, 25);
	}

	private void botonVisualizarDatos() {
		ActionListener listener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaVisualizarDatosCargados ventanaVisualizarDatos = new VentanaVisualizarDatosCargados(
						listaDePersonas);
				ventanaVisualizarDatos.setVisible(true);
			}
		};
		Common.agregarBoton(menuPrincipal, "Visualizar datos cargados", listener, 98, 149, 224, 25);
	}

	private void botonCrearGrupos() {
		ActionListener listener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listaDePersonas.cantidadPersonasEnLista() < cantidadDeGrupos) {
					ventanaDeError();
					return;
				}
				VentanaVisualizarGrupos ventanaGrupos = new VentanaVisualizarGrupos(listaDePersonas, cantidadDeGrupos);
				ventanaGrupos.setVisible(true);
			}

		};
		Common.agregarBoton(menuPrincipal, "Crear Grupos", listener, 98, 186, 224, 25);
	}

	private void crearLabels() {
		titulo();
		descripcion();
	}

	private void titulo() {
		Common.agregarLabel(menuPrincipal, "Clustering", 168, 12, 90, 30);
	}

	private void descripcion() {
		Common.agregarLabel(menuPrincipal,
				"<html>Clustering es un programa encargado de organizar<br/>personas en distintos grupos segun su grado de afinidad</html>",
				28, 39, 385, 43);
	}

	private void inicializarMenuCantidadDeGrupos() {
		JComboBox<Integer> menuCantidadDeGrupos = new JComboBox<>();
		menuCantidadDeGrupos.addItem(2); /*-parte desde 2 grupos*/
		menuCantidadDeGrupos.addItem(3);
		menuCantidadDeGrupos.addItem(4);
		menuCantidadDeGrupos.setBounds(328, 187, 70, 23);
		menuPrincipal.getContentPane().add(menuCantidadDeGrupos);
		menuCantidadDeGrupos.setToolTipText("Seleccione cantidad de grupos para crear");
		ToolTipManager.sharedInstance().setInitialDelay(50);

		menuCantidadDeGrupos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integer numeroSeleccionado = (Integer) menuCantidadDeGrupos.getSelectedItem();
				cantidadDeGrupos = numeroSeleccionado;
			}
		});
	}

	private void ventanaDeError() {
		Common.mostrarMensajeDeError(
				"No hay suficientes personas ingresadas para crear " + cantidadDeGrupos + " grupo(s)", "Error");
	}
}
