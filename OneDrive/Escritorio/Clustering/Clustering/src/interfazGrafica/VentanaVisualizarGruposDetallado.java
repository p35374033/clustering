package interfazGrafica;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import clustering.ListaDePersonas;
import clustering.Persona.Intereses;

public class VentanaVisualizarGruposDetallado extends JFrame {

	private ListaDePersonas grupo;

	public VentanaVisualizarGruposDetallado(ListaDePersonas grupo) {
		Common.inicializarVentana(this);

		this.grupo = grupo;
		crearVentanaVisualizarGrupoDetallado();
	}

	private void crearVentanaVisualizarGrupoDetallado() {
		agregarLabels();
		agregarTextos();
		botonCerrar();
	}

	private void agregarLabels() {
		int cantidadDeIntegrantes = grupo.cantidadPersonasEnLista();
		Common.agregarLabel(this, "Cantidad de integrantes: " + cantidadDeIntegrantes, 2, 12, 311, 21);
		Common.agregarLabel(this, "Mayor Interes: " + grupo.interesPrincipal(), 12, 141, 127, 15);
	}

	private void agregarTextos() {
		textoIntegrantes();
		textoDescripcion();
	}

	private void textoIntegrantes() {
		String integrantes = visualizarDatos(grupo);
		Common.agregarCuadroDeTexto(this, integrantes, 12, 69, 416, 60);
	}

	private void textoDescripcion() {
		List<Intereses> intereses = new ArrayList<>(
				List.of(Intereses.CIENCIAS, Intereses.DEPORTES, Intereses.ESPECTACULOS, Intereses.MUSICA));
		StringBuilder builder = new StringBuilder();
		for (Intereses interes : intereses) {
			float promedio = grupo.promedioInteres(interes);
			String promedioInteres = String.format("Interes promedio por %s: %.2f\n", interes, promedio);
			builder.append(promedioInteres);
		}
		Common.agregarCuadroDeTexto(this, builder.toString(), 12, 168, 416, 60);
	}

	private void botonCerrar() {
		Common.agregarBotonCerrar(this);
	}

	private String visualizarDatos(ListaDePersonas grupo) {

		StringBuilder personas = new StringBuilder();
		for (int i = 0; i < this.grupo.cantidadPersonasEnLista(); i++) {
			personas.append(this.grupo.obtenerPersona(i).getNombre());
			personas.append(": ");
			personas.append(" Musica: ");
			personas.append(this.grupo.obtenerPersona(i).getMusica());
			personas.append(" Deportes: ");
			personas.append(this.grupo.obtenerPersona(i).getDeportes());
			personas.append(" Espectaculos: ");
			personas.append(this.grupo.obtenerPersona(i).getEspectaculos());
			personas.append(" Ciencia: ");
			personas.append(this.grupo.obtenerPersona(i).getCiencias());
			personas.append("\n");

		}
		String lista = personas.toString();
		return lista;

	}
}
