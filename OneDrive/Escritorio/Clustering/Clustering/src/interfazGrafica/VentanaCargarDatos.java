package interfazGrafica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.JTextField;

import clustering.ListaDePersonas;
import clustering.Persona;
import clustering.Persona.Intereses;

public class VentanaCargarDatos extends JFrame {

	private static final int INTERES_POR_DEFAULT = 5;

	private HashMap<String, Integer> intereses;

	private String nombre;

	private ListaDePersonas lista;

	public VentanaCargarDatos(ListaDePersonas listaPersonas) {
		Common.inicializarVentana(this);

		this.lista = listaPersonas;
		this.intereses = new HashMap<String, Integer>();
		for (Intereses interes : Intereses.values()) {
			intereses.put(interes.name(), INTERES_POR_DEFAULT);
		}

		crearVentanaCargarDatos();
	}

	private void crearVentanaCargarDatos() {
		cuadroDeTextoNombre();
		agregarLabels();
		agregarSliders();
		botonConfirmarDatos();
	}

	private void cuadroDeTextoNombre() {
		JTextField textoNombre = new JTextField();
		textoNombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				nombre = textoNombre.getText();
			}
		});
		Common.cuadroInputUsuario(this, textoNombre, 170, 60, 114, 19);
	}

	private void agregarLabels() {
		Common.agregarLabel(this,
				"<html>Ingrese el nombre de la persona y el nivel de interes<br/> sobre los siguientes temas<html>", 22,
				12, 380, 37);
		Common.agregarLabel(this, "Ingrese el nombre:", 22, 60, 146, 15);
		Common.agregarLabel(this, "Ningún interés", 104, 99, 114, 15);
		Common.agregarLabel(this, "Mucho interés", 288, 99, 114, 15);
		agregarLabelsCategorias();
	}

	private void agregarLabelsCategorias() {
		int posicionY = 127;
		int desplazamiento = 27;
		for (Intereses interes : Intereses.values()) {
			agregarLabelCategoria(interes.name(), posicionY);
			posicionY += desplazamiento;
		}
	}

	private void agregarSliders() {
		int posicionY = 127;
		int desplazamiento = 27;
		for (Intereses interes : Intereses.values()) {
			agregarSlider(interes, posicionY);
			posicionY += desplazamiento;
		}
	}

	private void agregarSlider(Intereses interes, int posicionY) {
		JSlider slider = new JSlider();
		slider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				intereses.put(interes.name(), slider.getValue());
			}
		});
		Common.agregarSlider(this, slider, INTERES_POR_DEFAULT, 150, posicionY, 200, 16);
	}

	private void agregarLabelCategoria(String nombre, int posAltura) {
		Common.agregarLabel(this, nombre, 22, posAltura, 100, 15);
	}

	private void botonConfirmarDatos() {
		ActionListener listener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (nombre == null || nombre.isBlank() || nombre.isEmpty()) {
					crearVentanaDeErrorNombreNulo();
				} else if (lista.existePersona(nombre)) {
					crearVentanaDeErrorPersonaYaEnLista();
				} else {
					crearPersona();
					dispose();
				}
			}
		};
		Common.agregarBoton(this, "Confirmar datos", listener, 160, 233, 159, 25);
	}

	private void crearPersona() {
		int interesDeportes = intereses.get(Intereses.DEPORTES.name());
		int interesMusica = intereses.get(Intereses.MUSICA.name());
		int interesEspectaculos = intereses.get(Intereses.ESPECTACULOS.name());
		int interesCiencia = intereses.get(Intereses.CIENCIAS.name());

		Persona persona = new Persona(nombre, interesDeportes, interesMusica, interesEspectaculos, interesCiencia);
		lista.agregarPersonaALista(persona);
	}

	private void crearVentanaDeErrorPersonaYaEnLista() {
		Common.mostrarMensajeDeError("Los datos de esta persona ya están cargados", "Ingrese otro nombre");
	}

	private void crearVentanaDeErrorNombreNulo() {
		Common.mostrarMensajeDeError("Debe ingresar un nombre para continuar", "Ingrese nombre");
	}
}
