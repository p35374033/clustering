package interfazGrafica;

import javax.swing.JFrame;

import clustering.ListaDePersonas;

public class VentanaVisualizarDatosCargados extends JFrame {

	private ListaDePersonas lista;

	public VentanaVisualizarDatosCargados(ListaDePersonas listaPersonas) {
		Common.inicializarVentana(this);
		this.lista = listaPersonas;

		crearVentanaCargarDatos();
	}

	private void crearVentanaCargarDatos() {
		botonCerrar();
		cuadroDeTexto();
	}

	private void botonCerrar() {
		Common.agregarBotonCerrar(this);
	}

	private void cuadroDeTexto() {
		String datos = visualizarDatos();
		Common.agregarCuadroDeTexto(this, datos, 12, 12, 396, 200);

	}

	private String visualizarDatos() {

		StringBuilder personas = new StringBuilder();
		for (int i = 0; i < this.lista.cantidadPersonasEnLista(); i++) {
			personas.append(this.lista.obtenerPersona(i).getNombre());
			personas.append(": ");
			personas.append(" Musica: ");
			personas.append(this.lista.obtenerPersona(i).getMusica());
			personas.append(" Deportes: ");
			personas.append(this.lista.obtenerPersona(i).getDeportes());
			personas.append(" Espectaculos: ");
			personas.append(this.lista.obtenerPersona(i).getEspectaculos());
			personas.append(" Ciencia: ");
			personas.append(this.lista.obtenerPersona(i).getCiencias());
			personas.append("\n");

		}
		String lista = personas.toString();
		return lista;

	}

}
