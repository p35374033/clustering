package interfazGrafica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Common {

	public static void agregarLabel(JFrame frame, String nombre, int x, int y, int width, int heigth) {
		JLabel label = new JLabel(nombre);
		label.setBounds(x, y, nombre.length() * 10, heigth);
		frame.getContentPane().add(label);
	}

	public static void agregarSlider(JFrame frame, JSlider slider, int valorInicial, int x, int y, int width,
			int heigth) {
		slider.setMinimum(1);
		slider.setMaximum(5);
		slider.setValue(valorInicial);
		slider.setBounds(x, y, width, heigth);
		frame.getContentPane().add(slider);
	}

	public static void agregarBoton(JFrame frame, String nombre, ActionListener listener, int x, int y, int width,
			int heigth) {
		JButton boton = new JButton(nombre);
		boton.addActionListener(listener);
		boton.setBounds(x, y, width, heigth);
		frame.getContentPane().add(boton);
	}

	public static void cuadroInputUsuario(JFrame frame, JTextField textField, int x, int y, int width, int heigth) {
		textField.setBounds(x, y, width, heigth);
		frame.getContentPane().add(textField);
	}

	public static void inicializarVentana(JFrame frame) {
		Common.inicializarVentana(frame, 450);
	}

	public static void inicializarVentana(JFrame frame, int ancho) {
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setBounds(100, 100, ancho, 300);
		frame.setContentPane(contentPane);
		frame.getContentPane().setLayout(null);
	}

	public static void mostrarMensajeDeError(String mensaje, String titulo) {
		JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.ERROR_MESSAGE);
	}

	public static void agregarCuadroDeTexto(JFrame frame, String texto, int x, int y, int width, int heigth) {
		JTextArea area = new JTextArea();
		area.setText(texto);
		area.setLineWrap(true);
		area.setWrapStyleWord(true);
		area.setEditable(false);
		area.setOpaque(false);
		area.setBounds(x, y, width, heigth);
		frame.getContentPane().add(area);
		JScrollPane scrollPane = new JScrollPane(area);
		scrollPane.setBounds(x, y, width, heigth);
		frame.getContentPane().add(scrollPane);
	}

	public static void agregarBotonCerrar(JFrame frame) {
		ActionListener listener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		};
		agregarBoton(frame, "Cerrar", listener, (frame.getWidth() / 2) - (117 / 2), 233, 117, 25);
	}

}
