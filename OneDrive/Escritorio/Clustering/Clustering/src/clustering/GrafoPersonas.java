package clustering;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class GrafoPersonas {

	private final static int ARISTA_INEXISTENTE = -1;

	private ListaDePersonas listaPersonas;

	private Grafo grafo; /*-grafo generico-*/

	public GrafoPersonas(ListaDePersonas listaPersonas) {
		verificarCantidadDePersonasEnLista(listaPersonas);
		this.listaPersonas = listaPersonas;
		construirMatriz(listaPersonas);
	}

	public GrafoPersonas(ListaDePersonas listaPersonas, Grafo grafo) {
		verificarCantidadDePersonasEnLista(listaPersonas);
		this.listaPersonas = listaPersonas;
		this.grafo = grafo;
	}

	public void agregarPersonaGrafo(Persona persona) {
		listaPersonas.agregarPersonaALista(persona);
		construirMatriz(listaPersonas);
	}

	public void eliminarRelacionConDiferenciaIndiceMasAmplio() {
		int personaUno = 0, personaDos = 0, aristaMayorPeso = ARISTA_INEXISTENTE;
		for (int i = 0; i < tamanio(); i++) {
			for (int j = 0; j < tamanio(); j++) {
				if (i != j) {
					int indice = obtenerIndiceSimilaridad(i, j);
					if (indice > aristaMayorPeso) {
						aristaMayorPeso = indice;
						personaUno = i;
						personaDos = j;
					}
				}
			}
		}
		grafo.eliminarArista(personaUno, personaDos);
	}

	public Set<ListaDePersonas> separarEnGrupos(int cantidadDeGrupos) {
		int maximoGrupos = tamanio();
		if (cantidadDeGrupos > maximoGrupos) {
			throw new IllegalArgumentException(
					"Por el tamaño del grafo se pueden tener una cantidad maxima de hasta " + maximoGrupos + " grupos");
		}
		int cantidadDeAristasAEliminar = cantidadDeGrupos - 1;
		/*
		 * 1. Construir un grafo completo G con un vértice por cada persona, una arista
		 * entre cada par de personas, y de modo tal que cada arista tiene peso igual al
		 * ´ındice de similaridad entre las dos personas.
		 */
		GrafoPersonas grafoCompleto = new GrafoPersonas(this.listaPersonas);

		// 2. Construir un ´arbol generador mínimo T de G.
		GrafoPersonas grafo = grafoCompleto.obtenerArbolGeneradorMinimoDePersonas();

		// 3. Eliminar la arista de mayor peso del árbol T.
		for (int i = 0; i < cantidadDeAristasAEliminar; i++) {
			grafo.eliminarRelacionConDiferenciaIndiceMasAmplio();
		}

		return grafo.obtenerGrupos(cantidadDeGrupos);
	}

	private Set<ListaDePersonas> obtenerGrupos(int cantidadDeGrupos) {
		HashMap<Integer, ListaDePersonas> grupos = new HashMap<>();
		for (int i = 0; i < cantidadDeGrupos; i++) {
			grupos.put(i, new ListaDePersonas());
		}

		int personaInicial = elegirNroPersonaRandom();

		// En persona pivot pondremos las personas hacia donde buscaremos las relaciones
		// de otras personas. Si se puede llegar de una persona A a una persona B,
		// recorriendo el grafo, quiere decir que pertenecen al mismo grupo
		Integer[] personaPivotDeGrupo = new Integer[cantidadDeGrupos];
		int ultimoGrupoInicializado = 0;
		personaPivotDeGrupo[ultimoGrupoInicializado] = personaInicial;

		// vamos recorriendo todos los vertices
		for (int persona = 0; persona < tamanio(); persona++) {
			boolean seSumoAUnGrupo = false;

			// Busco a que grupo pertenece la persona
			for (int personaPivot = 0; personaPivot < personaPivotDeGrupo.length; personaPivot++) {

				// Si no existe ninguna persona aun en el grupo, se saltea el mismo
				Integer nroPersona = personaPivotDeGrupo[personaPivot];
				if (Objects.isNull(nroPersona)) {
					continue;
				}
				boolean seLlegaAlVerticeBuscado = grafo.existeCaminoEntreVertices(persona,
						personaPivotDeGrupo[personaPivot]);

				// Si se llega, quiere decir que pertenece a este grupo
				if (seLlegaAlVerticeBuscado) {
					Persona p = listaPersonas.obtenerPersona(persona);
					grupos.get(personaPivot).agregarPersonaALista(p);
					// para validar si el vertice actual fue agregado a algun grupo o debera
					// asignarse a uno nuevo
					seSumoAUnGrupo = true;
				}
			}

			// en caso de que no llegó a ninguno de los grupos existentes, se pone como
			// personaPivot de un nuevo grupo
			if (seSumoAUnGrupo == false) {
				++ultimoGrupoInicializado;
				personaPivotDeGrupo[ultimoGrupoInicializado] = persona;
				Persona p = listaPersonas.obtenerPersona(persona);
				grupos.get(ultimoGrupoInicializado).agregarPersonaALista(p);
			}
		}

		HashSet<ListaDePersonas> out = new HashSet<>();
		grupos.forEach((grupo, personas) -> {
			ListaDePersonas lista = new ListaDePersonas();
			for (Persona p : personas.getPersonas()) {
				lista.agregarPersonaALista(p);
			}
			out.add(lista);
		});

		return out;
	}

	public boolean existeRelacion(int persona1, int persona2) {
		return grafo.existeArista(persona1, persona2);
	}

	public int tamanio() {
		return grafo.tamanio();
	}

	public Integer obtenerIndiceSimilaridad(int i, int j) {
		return grafo.obtenerArista(i, j);
	}

	public ListaDePersonas getListaPersonas() {
		return this.listaPersonas;
	}

	private void construirMatriz(ListaDePersonas personas) {
		grafo = new Grafo(listaPersonas.cantidadPersonasEnLista());
		for (int i = 0; i < personas.cantidadPersonasEnLista(); i++) {
			for (int j = 0; j < personas.cantidadPersonasEnLista(); j++) {
				if (i != j) {
					int indiceSimilaridad = personas.obtenerPersona(i).indiceSimilaridad(personas.obtenerPersona(j));
					grafo.agregarArista(indiceSimilaridad, i, j);
				}
			}
		}
	}

	private GrafoPersonas obtenerArbolGeneradorMinimoDePersonas() {
		return new GrafoPersonas(listaPersonas, grafo.obtenerArbolGeneradorMinimo());
	}

	private int elegirNroPersonaRandom() {
		int vertice = (int) (Math.random() * tamanio());
		return vertice;
	}

	private void verificarCantidadDePersonasEnLista(ListaDePersonas list) {
		if (list.cantidadPersonasEnLista() < 2) {
			throw new IllegalArgumentException("se necesitan mínimo 2 personas en la lista");
		}
	}

}
