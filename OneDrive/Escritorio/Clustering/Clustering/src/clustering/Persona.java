package clustering;

import java.util.HashMap;
import java.util.Objects;

public class Persona {

	private String nombre;

	private HashMap<String, Integer> intereses;

	public Persona(String nombre, int interesDeportes, int interesMusica, int interesEspectaculos,
			int interesCiencias) {
		verificarValores(nombre, interesDeportes, interesMusica, interesEspectaculos, interesCiencias);

		this.nombre = nombre;

		intereses = new HashMap<String, Integer>(Intereses.values().length);
		intereses.put(Intereses.DEPORTES.name(), interesDeportes);
		intereses.put(Intereses.MUSICA.name(), interesMusica);
		intereses.put(Intereses.ESPECTACULOS.name(), interesEspectaculos);
		intereses.put(Intereses.CIENCIAS.name(), interesCiencias);
	}

	public int indiceSimilaridad(Persona p) {
		int indiceDeportes = Math.abs(getDeportes() - p.getDeportes());
		int indiceMusica = Math.abs(getMusica() - p.getMusica());
		int indiceEspectaculos = Math.abs(getEspectaculos() - p.getEspectaculos());
		int indiceCiencias = Math.abs(getCiencias() - p.getCiencias());

		return indiceDeportes + indiceMusica + indiceEspectaculos + indiceCiencias;
	}

	private void verificarValores(String nombre, int dep, int mus, int esp, int ci) {
		verificarNombre(nombre);
		verificarIntereses(dep, mus, esp, ci);
	}

	private void verificarNombre(String nombre) {
		if (Objects.isNull(nombre) || nombre.isBlank() || nombre.isEmpty())
			throw new IllegalArgumentException("Debe ingresar un nombre");
	}

	private void verificarIntereses(int dep, int mus, int esp, int ci) {
		if (dep < 1 || mus < 1 || esp < 1 || ci < 1)
			throw new IllegalArgumentException("No se admiten valores menores a 1");
		if (dep > 5 || mus > 5 || esp > 5 || ci > 5)
			throw new IllegalArgumentException("No se admiten valores mayores a 5");
	}

	public String getNombre() {
		return this.nombre;
	}

	public int getDeportes() {
		return intereses.get(Intereses.DEPORTES.name());
	}

	public int getMusica() {
		return intereses.get(Intereses.MUSICA.name());
	}

	public int getEspectaculos() {
		return intereses.get(Intereses.ESPECTACULOS.name());
	}

	public int getCiencias() {
		return intereses.get(Intereses.CIENCIAS.name());
	}

	public enum Intereses {
		DEPORTES, MUSICA, ESPECTACULOS, CIENCIAS;
	}

}
