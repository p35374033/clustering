package clustering;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import clustering.Persona.Intereses;

public class ListaDePersonas {

	private List<Persona> listaPersonas = new LinkedList<>();

	public ListaDePersonas() {
	}

	public void agregarPersonaALista(Persona p) {
		listaPersonas.add(p);
	}

	public int cantidadPersonasEnLista() {
		return listaPersonas.size();
	}

	public List<Persona> getPersonas() {
		return listaPersonas;
	}

	public void setListaPersonas(List<Persona> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}

	public Persona obtenerPersona(int i) {
		if (i < 0 || i >= cantidadPersonasEnLista()) {
			throw new IllegalArgumentException("indice fuera de rango");
		}
		return listaPersonas.get(i);
	}

	public Persona obtenerPersonaPorNombre(String nombre) {
		if (Objects.isNull(nombre) || nombre.isBlank() || nombre.isEmpty())
			throw new IllegalArgumentException("Debe ingresar un nombre");
		for (Persona p : listaPersonas) {
			if (p.getNombre().equals(nombre))
				return p;
		}
		return null;
	}

	public boolean existePersona(String nombre) {
		for (Persona persona : listaPersonas) {
			if (persona.getNombre().equalsIgnoreCase(nombre)) {
				return true;
			}
		}
		return false;
	}

	public float deportesPromedio() {
		Float promedio;
		int suma = 0;
		for (Persona persona : listaPersonas) {
			suma = suma + persona.getDeportes();
		}
		promedio = (float) suma / listaPersonas.size();

		return promedio;
	}

	public float espectaculosPromedio() {
		Float promedio;
		int suma = 0;
		for (Persona persona : listaPersonas) {
			suma = suma + persona.getEspectaculos();
		}
		promedio = (float) suma / listaPersonas.size();

		return promedio;
	}

	public float musicaPromedio() {
		Float promedio;
		int suma = 0;
		for (Persona persona : listaPersonas) {
			suma = suma + persona.getMusica();
		}
		promedio = (float) suma / listaPersonas.size();

		return promedio;
	}

	public float cienciaPromedio() {
		Float promedio;
		int suma = 0;
		for (Persona persona : listaPersonas) {
			suma = suma + persona.getCiencias();
		}
		promedio = (float) suma / listaPersonas.size();

		return promedio;
	}

	public float promedioInteres(Intereses interes) {
		if (interes.equals(Intereses.CIENCIAS)) {
			return cienciaPromedio();
		}

		if (interes.equals(Intereses.MUSICA)) {
			return musicaPromedio();
		}

		if (interes.equals(Intereses.ESPECTACULOS)) {
			return espectaculosPromedio();
		}

		return deportesPromedio();
	}

	public Intereses interesPrincipal() {
		float ciencia = this.cienciaPromedio();
		float musica = this.musicaPromedio();
		float espectaculos = this.espectaculosPromedio();
		float deportes = this.deportesPromedio();
		float maximo1 = Math.max(ciencia, musica);
		float maximo2 = Math.max(espectaculos, deportes);
		float maximo3 = Math.max(maximo1, maximo2);
		if (maximo3 == ciencia) {
			return Intereses.CIENCIAS;
		}
		if (maximo3 == musica) {
			return Intereses.MUSICA;
		}
		if (maximo3 == espectaculos) {
			return Intereses.ESPECTACULOS;
		}
		return Intereses.DEPORTES;

	}

}
