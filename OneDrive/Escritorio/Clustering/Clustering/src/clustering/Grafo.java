package clustering;

import java.util.HashSet;
import java.util.Set;

public class Grafo {

	private final static int ARISTA_INEXISTENTE = -1;

	private int[][] matriz; /*-mat de adyacencia-*/

	public Grafo(int vertices) {
		matriz = new int[vertices][vertices];
		for (int i = 0; i < vertices; i++) {
			for (int j = 0; j < vertices; j++) {
				matriz[i][j] = ARISTA_INEXISTENTE;
			}
		}
	}

	/*
	 * 
	 * Obtener Arbol Generador Mínimo a partir de una implementación del algoritmo
	 * de Prim
	 * 
	 */
	public Grafo obtenerArbolGeneradorMinimo() {
		int vertices = tamanio();
		Grafo arbolGeneradorMinimo = new Grafo(vertices);

		// se iran agregando los vertices visitados
		boolean[] verticesVisitados = new boolean[vertices];

		// partimos de un vertice al azar y lo "visitamos"
		int verticePrimeroAVisitar = elegirVerticeRandom();
		verticesVisitados[verticePrimeroAVisitar] = true;

		int cantidadDeVerticesDelArbol = 1; // el verticePrimeroAVisitar

		// se itera hasta que el arbol tenga todos los vertices
		while (cantidadDeVerticesDelArbol < tamanio()) {
			// inicializamos variables que se irán reutilizando
			int pesoMinimoEncontrado = Integer.MAX_VALUE;
			int x = 0;
			int y = 0;
			int nuevoVerticeVisitado = 0;
			/*
			 * a partir de nodos visitados debemos buscar la arista de menor peso hacia los
			 * nodos no visitados
			 */
			// Busco un nodo ya visitado
			for (int vertice = 0; vertice < tamanio(); vertice++) {

				// Si no está visitado el nodo del cual partir, se descarta.
				if (!verticesVisitados[vertice])
					continue;

				/*
				 * Solo me interesan las aristas hacia nodos NO visitados.
				 */
				for (Integer vecino : vecinos(vertice)) {
					/*
					 * En caso de que el vecino del nodo del cual parti ya está visitado, se
					 * descarta
					 */
					if (verticesVisitados[vecino])
						continue;

					int pesoArista = obtenerArista(vertice, vecino);

					// Me quedo con el peso de la arista, y el potencial nuevo vertice visitado
					if (pesoArista < pesoMinimoEncontrado) {
						pesoMinimoEncontrado = pesoArista;
						x = vertice;
						y = vecino;
						nuevoVerticeVisitado = vecino;
					}
				}
			}
			/*
			 * visito el nuevo vertice final encontrado y agrego la arista al nuevo grafo de
			 * arbol generador minimo
			 */
			verticesVisitados[nuevoVerticeVisitado] = true;
			arbolGeneradorMinimo.agregarArista(pesoMinimoEncontrado, x, y);
			cantidadDeVerticesDelArbol++;
		}
		return arbolGeneradorMinimo;
	}

	public Set<Integer> vecinos(int vertice) {
		verticeValido(vertice);

		Set<Integer> ret = new HashSet<Integer>();

		for (int i = 0; i < tamanio(); i++) {
			if (vertice != i && existeArista(vertice, i)) {
				ret.add(i);
			}
		}
		return ret;
	}

	public boolean existeCaminoEntreVertices(int verticeOrigen, int verticeDestino) {
		boolean[] verticesVisitados = new boolean[tamanio()];
		return existeCaminoEntreVertices(verticesVisitados, verticeOrigen, verticeDestino);

	}

	private boolean existeCaminoEntreVertices(boolean[] verticesVisitados, int vertice, int verticeBuscado) {
		boolean llegaAlVerticeBuscado = false;
		verticesVisitados[vertice] = true;
		if (vertice == verticeBuscado) {
			return true;
		}
		Set<Integer> vecinos = vecinos(vertice);
		for (Integer vecino : vecinos) {
			if (!verticesVisitados[vecino]) {
				llegaAlVerticeBuscado = llegaAlVerticeBuscado
						|| existeCaminoEntreVertices(verticesVisitados, vecino, verticeBuscado);
			}
		}
		return llegaAlVerticeBuscado;
	}

	public void agregarArista(int peso, int verticeA, int verticeB) {
		verificarPosiciones(verticeA, verticeB);
		matriz[verticeA][verticeB] = peso;
		matriz[verticeB][verticeA] = peso;
	}

	public void eliminarArista(int verticeA, int verticeB) {
		verificarPosiciones(verticeA, verticeB);
		matriz[verticeA][verticeB] = ARISTA_INEXISTENTE;
		matriz[verticeB][verticeA] = ARISTA_INEXISTENTE;
	}

	public boolean existeArista(int x, int y) {
		verificarPosiciones(x, y);
		return this.matriz[x][y] != ARISTA_INEXISTENTE;
	}

	public int obtenerArista(int i, int j) {
		verificarPosiciones(i, j);
		return matriz[i][j];
	}

	private void verticeValido(int vertice) {
		if (vertice >= tamanio()) {
			throw new IllegalArgumentException("vertice no existe");
		}
	}

	public int tamanio() {
		return matriz.length;
	}

	private void verificarPosiciones(int x, int y) {
		verificarXYiguales(x, y);
		verificarXYNegativos(x, y);
		verificarXYexcedeTamanio(x, y);
	}

	private void verificarXYNegativos(int x, int y) {
		if (x < 0 || y < 0)
			throw new IllegalArgumentException("i e j deben ser positivos");
	}

	private void verificarXYiguales(int x, int y) {
		if (x == y)
			throw new IllegalArgumentException("Estos valores no deben ser iguales (No Loops)");
	}

	private void verificarXYexcedeTamanio(int x, int y) {
		if (x >= tamanio() || y >= tamanio())
			throw new IllegalArgumentException("valor/es excede/n límites");
	}

	private int elegirVerticeRandom() {
		int vertice = (int) (Math.random() * tamanio());
		return vertice;
	}
}
